let category = ['¿Qué comida te gusta que mucha gente cree que es un poco extraña?', ' ¿Cuáles son tus videojuegos favoritos?','¿Cuál es el dato curioso más increíble que conoces?', '¿Cuál sería para ti el clima perfecto?', '¿Cuándo eres más productivo: en la mañana o en la noche?', 'Si tuvieras que hacer una presentación de 20 minutos sin prepararte, ¿sobre qué hablarías?', '¿Qué cosas crees que muchas personas se están perdiendo porque no saben que existe?', '¿Crees que dependes mucho de tu teléfono celular? ¿Por qué o por qué no?', ' ¿Qué cosa sueles recomendar a la gente que conoces?', '¿Qué incluye tu desayuno favorito?', 'Si pudieras elegir tus sueños, ¿qué te gustaría soñar?', ' ¿Cuál ha sido el libro más significativo que has leido?' ]; 


function displayCategory() {
  let Category = category[Math.floor(Math.random() * category.length)];
  let showCard = `${Category}`;
  document.getElementById("showing").style.background = '#9e88c0';
  document.getElementById("showing").innerHTML = showCard;

}

let challenge = ['¿Qué materias crees que deberían enseñarse en la escuela?', '¿Qué cosa te cuesta hacer que vale completamente la pena?', '¿Qué sitio web o aplicación desearías que existiera?', '¿Qué cosa o situación te ha traído consecuencias?', '¿Qué te hubiera gustado aprender hace mucho tiempo?', '¿A qué clase de desafíos te enfrentas en la actualidad?', ' ¿Cuál fue la última cosa que te causó mucha emoción?', '¿Qué es algo que alguna vez fue importante, pero con el tiempo ha perdido su importancia?', '¿Contra qué te sueles rebelar?', '¿Cuál es la persona más inteligente o creativa que conoces?', ' ¿Qué cosa te hace perder mucho tiempo en tu día a día?']; 


function displayChallenge() {
  let Challenge = challenge[Math.floor(Math.random() * challenge.length)];
  let showCard = `${Challenge}`;
  document.getElementById("showing2").style.background = '#9e88c0';
  document.getElementById("showing2").innerHTML = showCard;

}
